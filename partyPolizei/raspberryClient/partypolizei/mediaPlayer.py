import os
import json
from socketIO_client import SocketIO, BaseNamespace
import pygame

mixer = pygame.mixer
mixer.init()

socketIO = SocketIO('169.254.0.1', 3000)
socket = socketIO.define(BaseNamespace, '/partyPolizei')
tracks = []
auto = False

def load_tracks():
	global tracks
	tracks = os.listdir("tracks")

def on_trackchange(*args):
	
	print(args[0]['value'])
	mixer.music.load("tracks/"+args[0]['value'])
	mixer.music.play()

def on_playback(*args):
	print(args[0]['value'])	
	if args[0]['value']==True:
		print("playing")
		mixer.music.unpause()
	else:
		mixer.music.pause()

def on_forward(*args):
	data = args[0]
	print ("forward: ",data)

def on_ready():
	load_tracks()
	print(tracks)
	socket.emit("tracksAvailable", tracks)
	socket.emit("ui_volume", mixer.music.get_volume())

def on_threshold(*args):
	print (auto)
	print("playback volume: ",mixer.music.get_volume())
	print("threshold")
	if auto==True:
		print("lowering volume")
		mixer.music.set_volume(mixer.music.get_volume()-0.00001)
	socket.emit("ui_volume", mixer.music.get_volume())		

def on_auto(*args):
	global auto
	auto = args[0]['value']
	print("automatic: ",auto)

def on_volume(*args):
	mixer.music.set_volume(args[0]['value'])

	
socket.on('forward', on_forward)
socket.on('connection', on_ready)
socket.on('ready', on_ready)
socket.on('trackchange', on_trackchange)
socket.on('playback', on_playback)
socket.on('threshold', on_threshold)
socket.on('auto', on_auto)
socket.on('volume', on_volume)

socketIO.wait()
