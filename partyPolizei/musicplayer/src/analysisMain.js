
var audioContext = null;
var meter = null;
var chart = null;
var canvasContext = null;
var WIDTH=500;
var HEIGHT=50;
var rafID = null;
var updateInterval = 100;
var socket = io('/partyPolizei');	


$(document).ready(function(){
	
    window.AudioContext = window.AudioContext || window.webkitAudioContext;
	
    // grab an audio context
    audioContext = new AudioContext();

    // Attempt to get audio input
    try {
        navigator.getUserMedia = 
        	navigator.getUserMedia ||
        	navigator.webkitGetUserMedia ||
        	navigator.mozGetUserMedia;

        // ask for an audio input
        navigator.getUserMedia(
        {
            "audio": {
                "mandatory": {
                    "googEchoCancellation": "false",
                    "googAutoGainControl": "false",
                    "googNoiseSuppression": "false",
                    "googHighpassFilter": "false"
                },
                "optional": []
            },
        }, gotStream, function(err){
			alert(err);
		});
    } catch (e) {
        alert('getUserMedia threw exception :' + e);
    }

});



var inputStream = null;

function gotStream(stream) {
    // Create an AudioNode from the stream.
    inputStream = audioContext.createMediaStreamSource(stream);

    loudnessProcessor = createLoudnessProcessor(audioContext);
	chart = createDbChart();
    inputStream.connect(loudnessProcessor);

	// update the chart view
	setInterval(function(){chart.updateChart()}, updateInterval); 
	socket.on("disconnect", function(){
		loudnessProcessor.shutdown();
	});

}


