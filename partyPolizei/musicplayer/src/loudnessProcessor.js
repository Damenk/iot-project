
/* define variables */
var bufferCount;
var bufferSum;
var leq;
var dbSum;
var avg;

/* intialize variables with stored values, if there are any. Else initialize with 0 */
if (localStorage["bufferCount"] != "" && ! (isNaN(localStorage["bufferCount"])))  {
	bufferCount = parseInt(localStorage["bufferCount"])
} else {
	bufferCount = 0;
}

if (localStorage["bufferSum"] != "" && ! (isNaN(localStorage["bufferSum"]))) {
	bufferSum = parseInt(localStorage["bufferSum"])
} else {
	bufferSum = 0;
}

if (localStorage["leq"] != "" && ! (isNaN(localStorage["leq"]))) {
	leq = parseFloat(localStorage["leq"])
} else {
	leq = 0;
}

if (localStorage["dbSum"] != "" && ! (isNaN(localStorage["dbSum"]))) {
	dbSum = parseFloat(localStorage["dbSum"])
} else {
	dbSum = 0;
}

if (localStorage["avg"] && ! (isNaN(localStorage["avg"]))) {
	avg = parseInt(localStorage["avg"])
} else {
	avg = 0;
}

console.log("stored: bc: ", localStorage["bufferCount"], "bs: ", localStorage["bufferSum"], "leq: ", localStorage["leq"], "dbSum: ", localStorage["dbSum"], "avg: ", localStorage["avg"])
console.log("actual: bc: ",bufferCount, "bs: ", bufferSum, "leq: ",leq, "dbSum: ", dbSum, "avg: ", avg)
var startTime = Date.now()
var startTimeTotal = Date.now();
function createLoudnessProcessor(audioContext, clipLevel, averaging) {
	var processor = audioContext.createScriptProcessor(8192);
	processor.onaudioprocess = volumeAudioProcess;
	processor.clipping = false;
	processor.lastClip = 0;
	processor.volume = 0;
	processor.averaging = averaging || 0.00;

	processor.connect(audioContext.destination);

	processor.shutdown =
	function () {
		this.disconnect();
		this.onaudioprocess = null;
	};

	return processor;
}

function volumeAudioProcess(event) {
	var input = event.inputBuffer.getChannelData(0);
	var bufferLength = input.length;
	var total = i = 0;
	var rms;
	var db;
	var now = Date.now();
	var elapsed = now - startTime;
	var elapsedTotal = now -startTimeTotal;

	if(elapsedTotal/1000 >10){
		console.log(dbSum, bufferSum, total);
		console.log("reset");
		bufferSum = 0;
		bufferCount = 0;
		dbSum = 0;
		
		startTimeTotal = Date.now();
	}
	bufferCount++;
	localStorage["bufferCount"] = bufferCount;
	while (i < bufferLength) {
		total += Math.abs(input[i++]);
	}
	
	//  take the square root of the sum.
	rms = Math.sqrt(total / bufferLength);

	this.db = (20 * Math.log10(rms));

	dbSum += this.db;
	localStorage["dbSum"] = dbSum;

	bufferSum += Math.pow(10, this.db / 10);
	localStorage["bufferSum"] = bufferSum;

	this.leq = 10 * Math.log10((1 / bufferCount) * bufferSum);
	localStorage["leq"] = this.leq;

	this.avg = dbSum / bufferCount;
	localStorage["avg"] = this.avg;

	if (this.leq > leqThreshold) {

		sendThreshold(elapsed, this.db);
		setTimeout(function () {
			$('.threshold-indicator').css('background-color', 'green');

			$('.bluelight').removeClass("brightness");
		}, 500);
		$('.bluelight').addClass("brightness");

	}else{
		$('.bluelight').addClass("brightness");
	}

	/* 	var avg =20*Math.log10( sumTotal/bufferCount) */

	/* console.log(db,leq, 'time: '+ Math.floor(bufferCount*0.186)); */
}

function sendThreshold(time, db) {
	if (time > 100) {
		socket.emit("threshold", {
			"value" : db
		});
		startTime = Date.now()
	}

}

