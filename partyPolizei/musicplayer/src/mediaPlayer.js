
$(document).ready(function () {

	/* Variables */
	var socket = io('/partyPolizei'),
	playback = false,
	playlist = $('#playlist'),
	play_pause = $('#play_pause'),
	reverse = $('#reverse'),
	forward = $('#forward'),
	tracks = [],
	currentTrack,
	lockedValues = [0, 0],
	tab1VolumeSlider = document.getElementById('volume1'),
	tab2VolumeSlider = document.getElementById('volume2');

	noUiSlider.create(tab1VolumeSlider, {
		start : 1,
		animate : false,
		range : {
			min : 0,
			max : 1
		}
	});

	noUiSlider.create(tab2VolumeSlider, {
		start : 1,
		animate : false,
		range : {
			min : 0,
			max : 1
		}
	});

	tab1VolumeSlider.noUiSlider.on('update', function (values, handle) {
		console.log(values[handle]);
		if(values[handle]<=0.1){
			$('#thresholdModal').modal('show'); 			
		}
		socket.emit("volume", {
			"value" : parseFloat(values[handle])
		});

	});

	
	tab1VolumeSlider.noUiSlider.on('slide', function(values, handle){
		tab2VolumeSlider.noUiSlider.set(values[handle])
	});
	
	tab2VolumeSlider.noUiSlider.on('slide', function(values, handle){
		tab1VolumeSlider.noUiSlider.set(values[handle])
	});
	

	/* Play/pause switch function */
	var togglePlayback = function () {
		console.log(playback);
		if (playback == false) {
			playback = true;
			play_pause.html("<i class=\"fa fa-pause\" aria-hidden=\"true\"></i>");
			socket.emit("playback", {
				value : true
			});

		} else {
			playback = false;
			play_pause.html("<i class=\"fa fa-play\" aria-hidden=\"true\"></i>") ;
			socket.emit("playback", {
				value : false
			});
		}
	}

	var updatePlaylist = function (currentTrack) {

		playlist.find(".active").removeClass("active");
		$(playlist.find('li').get(currentTrack)).addClass("active");

	}

	socket.on("ui_volume", function (data) {
		tab1VolumeSlider.noUiSlider.set(data);
		tab2VolumeSlider.noUiSlider.set(data)
	});

	socket.emit("ready");
	socket.on("playlist", function (data) {
		tracks = data;

		playlist.empty();

		for (var i = 0; i < tracks.length; i++) {
			var li = '<li class="list-group-item centerlist">' + tracks[i] + '</li>'
				playlist.append(li);
		}
	});

	/* Playlist trackselection */
	playlist.on("click", "li", function () {
		var li = $(this);
		currentTrack = li.index();
		play_pause.html("<i class=\"fa fa-pause\" aria-hidden=\"true\"></i>");
		socket.emit("trackchange", {
			'value' : currentTrack
		});
		updatePlaylist(currentTrack);
	});

	socket.on("currentTrack", function (data) {
		console.log(data);
		updatePlaylist(data)

	});

	/* play/pause*/
	play_pause.on("click",function(){
		togglePlayback();
		console.log(play_pause.html());
	});

	/* skip reverse emit */
	reverse.on("click", function () {
		if (currentTrack != 0) {
			currentTrack--;
		} else {
			currentTrack = tracks.length - 1
		}
		socket.emit('trackchange', {
			'value' : currentTrack
		});
		updatePlaylist(currentTrack)
	});

	/* skip forward */
	forward.on("click", function () {
		if (currentTrack < tracks.length - 1) {
			currentTrack++;
		} else {
			currentTrack = 0
		}
		socket.emit('trackchange', {
			'value' : currentTrack
		});
		updatePlaylist(currentTrack)
	});

});
