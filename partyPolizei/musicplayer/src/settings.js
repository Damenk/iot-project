var fastThreshold = 0;
var leqThreshold;

$(document).ready(function () {

	/*socketIO */
	var socket = io('/partyPolizei');
	/*calibrate button */
	var calibrate = $('#calibrate');
	/*automatic button */
	var auto = $('#auto');
	/*reset button */
	var reset = $('#reset');
	/*variables for calibrating the device */
	var dbCounter = 0;
	var calibrateThreshold = 0;
	var dbSum = 0;

	// get stored setting values if there are any
	if (localStorage["threshold"] != "") {
		leqThreshold = parseFloat(localStorage["threshold"]);
		$('#thresholdbox').val(leqThreshold.toFixed(2) + " db");

	} else {
		leqThreshold = 0;
		$('#thresholdbox').val((0.0).toFixed(2) + " db");
	}

	if (localStorage["auto"] == "true") {

		$('label[for="' + auto.attr('id') + '"]').addClass("active");
		console.log("auto", localStorage["auto"]);
		console.log("autobtnval ", auto.prop("checked"));

	}else{
		$('label[for="' + auto.attr('id') + '"]').removeClass("active");
	}

	var setCalibrateThreshold = function (db) {

		dbSum += db;
		dbCounter++;

		calibrateThreshold = dbSum / dbCounter;

	}

	/*automatic */
	auto.on("change", function () {
		localStorage["auto"] = auto.prop("checked");
		console.log("autoVal ", auto.prop("checked"));
		socket.emit("auto", {
			"value" : auto.prop('checked')
		})
	});

	 $('#calibrate').on("click", function () {
		var i = 0;
		dbSum = 0;
		dbCounter = 0;
		while (i < 50) {
			setInterval(function () {
				setCalibrateThreshold(loudnessProcessor.db)
			}, 100);

			i++;

		}
		chart.options.axisY.stripLines[0].value = calibrateThreshold;
		leqThreshold = calibrateThreshold;
		localStorage["threshold"] = calibrateThreshold;

		$('#thresholdbox').val(calibrateThreshold.toFixed(2) + " db");

	});
	reset.on("click", function () {
		leqThreshold = 0;
		localStorage["threshold"]= 0;
		
		bufferCount = 0;
		localStorage["bufferCount"] = 0;
		
		bufferSum = 0;
		localStorage["bufferSum"] = 0;
		
		dbSum = 0;
		localStorage["dbSum"] = 0;
		
		localStorage["auto"] = false;
		
		$('#thresholdbox').val((0).toFixed(2) + "db");
		socket.emit("auto", {
			"value" : false
		});
		$('label[for="' + auto.attr('id') + '"]').removeClass("active");
		chart.options.axisY.stripLines[0].value = 0;
	});


	$('#thresholdbox').on("change", function () {
		leqThreshold = $('#thresholdbox').val()
	});

	});
